import axios from 'axios'

const api = axios.create({
  baseURL: 'http://localhost:3000',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export function getUser () {
  return api.get('/users')
}

export function addUser () {
  return api.post('/users')
}

export default api
